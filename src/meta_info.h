
#ifndef META_INFO_H
#define META_INFO_H

#include "abstract_hardware_model.h"
#include <set>
#include "gpgpu-sim/scoreboard.h"
#include "gpgpu-sim/gpu-cache.h"

extern std::set<address_type> pc_indr_loads;
extern std::set<int> indr_loads;

extern std::set<Scoreboard*> scoreboards;
extern std::set<mshr_table*> mshrs;


#endif /* META_INFO_H */

