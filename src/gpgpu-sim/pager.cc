#include "pager.h"
#include <stdio.h>
#include <string.h>
#include "../meta_info.h"
#include "../trace.h"

std::set<address_type> pc_indr_loads;
std::set<Scoreboard*> scoreboards;
std::set<int> indr_loads;
std::set<mshr_table*> mshrs;

const int NUM_POLICIES = PAGE_MRU + 1;
static char const *policyNames[NUM_POLICIES] = {"None", "LRU", "RANDOM", "MRU"};

Policy a2policy(const char *p)
{
    for(int i=0; i<NUM_POLICIES; i++) {
        if(strcmp(p, policyNames[i]) == 0) {
            return static_cast<Policy>(i);
        }
    }
    assert(0 && "Illegal policy specification, exiting");
    return static_cast<Policy>(0);
}

Pager::Pager (unsigned int np, const char *pp, unsigned int ml, unsigned int ws)
{
	num_pages = np;
	paging_policy = a2policy(pp);
	miss_latency = ml;
	valid_pages = new std::deque<unsigned int>();
	window_size = ws;
	
	reset();
}

void Pager::reset()
{
    valid_pages->clear();
    reset_stats();
}

void Pager::access (dram_delay_t *d, unsigned int cycle_num, bool &page_miss)
{
    if(paging_policy == PAGE_NONE) {
        page_miss = false;
        return;
    }
    
    dram_accesses++;
    bool direct_load = (pc_indr_loads.count(d->req->get_pc()) == 0);
    if(direct_load) direct_loads++;
    
    
    unsigned int page_num = d->req->get_addr() >> 12; // divide by 4k
    bool already_valid;
    check_page(page_num, already_valid);
    if(already_valid) {
        page_miss = false;
        return;
    }
    
    if(direct_load) {
        // assume all direct loads hit
        add_valid_page(page_num);
        page_miss = false;
        return;
    }
    
    page_miss = true;
    dram_misses++;

    if(page_latencies.find(page_num) != page_latencies.end()) {
        // Already requested this page
        d->ready_cycle = page_latencies[page_num];
        
        std::deque<pending_page*>::iterator it;
        for(it=pending_reqs.begin(); it!=pending_reqs.end(); it++) {
            pending_page *pp = *it;
            if(pp->page_num == page_num) {
                pp->reqs->insert(d);
                JON_DEBUG(DBG_PAGING, "JON: (%u) Adding new request for page 0x%5x (sid%02d:wid%02d PC: 0x%x)\n",
                        cycle_num, page_num, d->req->get_sid(), d->req->get_wid(), d->req->get_pc());
                return;
            }
        }
    } else {
        page_latencies[page_num] = cycle_num + miss_latency;
    }
    
    // Issue new request for page
    // if pages around it are not in memory, put them in page_latencies as well
    for(unsigned int i=(page_num-window_size/2); i<=(page_num+window_size/2); i++) {
        if(page_latencies.find(i) == page_latencies.end()) {
            page_latencies[i] = cycle_num + miss_latency;
        }
    }
    d->ready_cycle = page_latencies[page_num];
    JON_DEBUG(DBG_PAGING, "JON: (%u) New page miss      for page 0x%5x (sid%02d:wid%02d PC: 0x%x)\n",
              cycle_num, page_num, d->req->get_sid(), d->req->get_wid(), d->req->get_pc());
    pending_page *new_page = new pending_page();
    new_page->page_num = page_num;
    new_page->reqs = new std::set<dram_delay_t*>();
    new_page->reqs->insert(d);
    pending_reqs.push_back(new_page);

    return;
}

void Pager::remove_page(){
    switch(paging_policy) {
        case PAGE_NONE:
            assert(0 && "remove_page() called on non-paged system");
            break;
        case PAGE_LRU:
            valid_pages->pop_back();
            break;
        case PAGE_RANDOM:
            int delete_idx;
            if(valid_pages->size() == 1) { //edge case
                delete_idx = 0;
            } else {
                delete_idx = rand() % (valid_pages->size()-1);
            }
            valid_pages->erase(valid_pages->begin()+delete_idx);
            break;
        case PAGE_MRU:
            valid_pages->pop_front();
            break;
    }
}

void Pager::check_page(unsigned int page_num, bool &already_valid) {
    already_valid = false;
    std::deque<unsigned int>::iterator it;
    for(it = valid_pages->begin(); it != valid_pages->end(); it++) {
        if(*it == page_num) {
            valid_pages->erase(it);
            valid_pages->push_front(page_num);
            already_valid = true;
            return;
        }
    }
}

void Pager::add_valid_page(unsigned int page_num) {
    bool already_valid;
    check_page(page_num, already_valid);
    if(!already_valid) {
        valid_pages->push_front(page_num);
    }
    if(valid_pages->size() > num_pages) {
        remove_page();
    }
}

void Pager::print_active_stats()
{
    std::set<Scoreboard*>::iterator sb_it;
    unsigned int active_warps = 0;
    for(sb_it=scoreboards.begin(); sb_it!=scoreboards.end(); sb_it++) {
        active_warps += (*sb_it)->num_active_warps();
//        (*sb_it)->printContents();
    }
    JON_DEBUG(DBG_PAGING,"JON: warps active: %u\n", active_warps);
}

void Pager::print_stall_stats()
{
    
    std::set<mshr_table*>::iterator mshr_it;
    std::map<unsigned, std::set<unsigned> > stalled_warps;
    for(mshr_it=mshrs.begin(); mshr_it!=mshrs.end(); mshr_it++) {
        std::deque<pending_page*>::iterator page_it;
        for(page_it=pending_reqs.begin(); page_it!=pending_reqs.end(); page_it++) {
            (*mshr_it)->warps_waiting((*page_it)->page_num, &stalled_warps);
//            (*mshr_it)->display(stdout);
        }
    }
    
    int stalled_count=0;
    if(stalled_warps.size()) {
        std::map<unsigned, std::set<unsigned> >::iterator it;
        for(it=stalled_warps.begin(); it!=stalled_warps.end(); it++) {
            stalled_count += it->second.size();
        }
        JON_DEBUG(DBG_PAGING,"JON: warps stalled: %d\n", stalled_count);
    }
    return;    
}

std::set<unsigned int> already_printed;

mem_fetch* Pager::pop_request(unsigned int cycle_num, unsigned int id) {
    // check if empty
    if(pending_reqs.empty()) {
        return NULL;
    }
    pending_page *pp = pending_reqs.front();
    if(pp->reqs->empty()) {
        JON_DEBUG(DBG_PAGING, "Error: Empty page\n");
        exit(1);
    } else {
        // if page hasn't come back, exit
        if((*pp->reqs->begin())->ready_cycle > cycle_num) {
            return NULL;
        }
        // if page has arrived, find a request associated with this bank
        for( std::set<dram_delay_t*>::iterator it=pp->reqs->begin();
                it != pp->reqs->end(); it++) {
            if((*it)->req->get_tlx_addr().chip == id) {
                mem_fetch* ret = (*it)->req;
                pp->reqs->erase(it);
                
                // only print stats and add pages for the first request of a given page
                bool first_response = (already_printed.count(pp->page_num) == 0);
                if(first_response) {
                    JON_DEBUG(DBG_PAGING, "JON: (%u) Service page miss 0x%5x PC: 0x%x\n", cycle_num, pp->page_num, ret->get_pc());
                    already_printed.insert(pp->page_num);
                    print_active_stats();
                    print_stall_stats();
                    for(unsigned int i=(pp->page_num-window_size/2); i<=(pp->page_num+window_size/2); i++) {
                        add_valid_page(i);
                        page_latencies.erase(i);
                    }
                }
                
                if(pp->reqs->empty()) {
                    already_printed.erase(pp->page_num);
                    pending_reqs.pop_front();
                }
                return ret;
            }
        }
    }
    return NULL;
}
void Pager::print_dram_stats()
{
    JON_DEBUG(DBG_PAGING, "DRAM Accesses / Direct Loads / Misses: (%u, %u, %u)\n", dram_accesses, direct_loads, dram_misses);
}

void Pager::reset_stats()
{
    dram_accesses = 0;
    dram_misses = 0;
    direct_loads = 0;    
}
