
#ifndef PAGER_H
#define PAGER_H

#include <deque>          // std::deque
#include <map>
#include <list>
#include <set>
#include "mem_fetch.h"


struct dram_delay_t
{
  unsigned long long ready_cycle;
  mem_fetch* req;
};
//struct dram_delay_comp {
//  bool operator()(const dram_delay_t *x, const dram_delay_t *y)
//  {return x->ready_cycle < y->ready_cycle;}
//};

struct pending_page
{
    unsigned int page_num;
    std::set<dram_delay_t*>* reqs;
};


typedef enum policy {PAGE_NONE, PAGE_LRU, PAGE_RANDOM, PAGE_MRU} Policy;

class Pager
{
public:
	Pager (unsigned int num_pages, const char *paging_policy, unsigned int miss_latency, unsigned int window_size);
	void access(dram_delay_t *d, unsigned int cycle_num, bool &page_miss);
        mem_fetch* pop_request(unsigned int cycle_num, unsigned int id);
        
	void print_dram_stats();
	
	void reset();
        void reset_stats();
private:
	
	unsigned int num_pages;
	Policy paging_policy;
	unsigned int miss_latency;
	std::deque<unsigned int> *valid_pages;
	unsigned int window_size;
	
	std::map<unsigned int, unsigned int> page_latencies;
        
        std::deque<pending_page*> pending_reqs;
        
        void add_valid_page(unsigned int page_num);
        void check_page(unsigned int page_num, bool &already_valid);
        void remove_page();
        void print_active_stats();
        void print_stall_stats();
	
	// for stats info
        
	unsigned int dram_accesses;
	unsigned int dram_misses;
        
        unsigned int direct_loads;
        
        
};


#endif
